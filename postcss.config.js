const postcssPresetEnv = require('postcss-preset-env')
// -
module.exports = {
	plugins: [
		require('autoprefixer'),
		postcssPresetEnv({
			browsers: 	"last 2 versions",
			stage: 		3,
			features: 	{
							"nesting-rules": false
						}
		}),
		require('cssnano')
	]
}