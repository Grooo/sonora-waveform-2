const { build } = require('esbuild')
// -

build({
    entryPoints: [
        'src/js/main.js'
    ],
    outfile: 	'build/js/main.min.js',
    plugins: 	[],
	bundle: 	true,
	write: 		true,
	minify: 	true,
	sourcemap: 	true,
	watch: 		true,
	target: 	[ 'es2017' ]
				
}).catch((error) => {
	console.error(error)
	process.exit(1)

}).then(result => {})