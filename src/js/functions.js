// - String
export function addZeros( n, length ){
    //- Length: Número de xifres que volem que tingui el nou número
    const str = (n > 0 ? n : -n) + "";
    let zeros = "";
    for (let i = length - str.length; i > 0; i--){
        zeros += "0";
    }
    zeros += str;
    return n >= 0 ? zeros : "-" + zeros;
}

// - Date
// - Converteix de milisegons a data llegible
export function time2obj( _time ){
    const o = {};
    if( _time < 0 ){
        o.negative = true;
       _time *= -1;
    }
    // -
    o.seconds = Math.floor((_time)/1000),
    o.minutes = Math.floor(o.seconds/60),
    o.hours   = Math.floor(o.minutes/60),
    o.days    = Math.floor(o.hours/24)
    //-
    o.hours   = o.hours-(o.days*24);
    o.minutes = o.minutes-(o.days*24*60)-(o.hours*60);
    o.seconds = o.seconds-(o.days*24*60*60)-(o.hours*60*60)-(o.minutes*60);
    //-
    return o;
}

export function timeInHTML(_time, _prefix = '' ){
    const o = time2obj(_time);
    let str = _prefix;
    if(o.days > 0) str += `${o.days}<span class="mini">d</span><span class="dots2">:</span>`;
    // - if(o.hours > 0)
    str += `${addZeros(o.hours, 2)}<span class="dots2">:</span>`;
    str += `${addZeros(o.minutes, 2)}<span class="dots2">:</span>`;
    str += addZeros(o.seconds, 2);
    return str;
}

export function timeInMinutes(_time){
    const o = time2obj(_time);
    let str = `${o.hours > 0 ? o.hours : ''}${o.hours > 0 ? ':' : ''}${o.minutes > 0 ? o.minutes : o.hours > 0 ? '00' : '0'} ${o.hours > 0 ? '' : ''}`;
    return str;
}

export function timeInHours(_time){
    const o = time2obj(_time);
    let str = `${o.hours > 0 ? o.hours : ''}${(o.hours > 0 && o.minutes > 0) ? ':' : ''}${o.minutes > 0 ? o.minutes : ''} ${o.hours > 0 ? ` hor${(o.hours == 1 && o.minutes == 0 ) ? 'a' : 'es'}` : ' minuts'}`;
    return str;
}

export function timeDifference( _date1, _date2 ){
    return Number(_date1) - Number(_date2);
}

export function date2Html(_date){
    const date = new Date(_date);
    return `${addZeros(date.getDate(), 2)}/${addZeros(date.getMonth()+1, 2)}/${date.getFullYear()}`;
}

export function currentTime2Html( _withSeconds){
    const date = new Date();
    let str = `${addZeros(date.getHours(), 2)}:${addZeros(date.getMinutes(), 2)}`;
    if( _withSeconds ) str += `:${addZeros(date.getSeconds(), 2)}`;
    return str;
}

export function dateFormated( _date ){
    return `${_date.getFullYear()}-${addZeros(_date.getMonth()+1, 2)}-${addZeros(_date.getDate(), 2)} ${addZeros(_date.getHours(), 2)}:${addZeros(_date.getMinutes(), 2)}:${addZeros(_date.getSeconds(), 2)}`;
}


// - Color -------------------------------------------
export function hexToRgbA(hex, alpha){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return `rgba(${[(c>>16)&255, (c>>8)&255, c&255].join(',')}, ${alpha})`;
    }
    throw new Error('Bad Hex');
}


// - Math -------------------------------------------
export function deg2rad(deg){
    return deg * (Math.PI/180)
}
export function rad2deg(rad, absolute){
	let deg = rad / (Math.PI/180);
	if( absolute && deg < 0 ) deg = 360 + deg;
  return deg;
}
export function polarToCartesian(centerX, centerY, radius, angleInDegrees){
    var angleInRadians = deg2rad(angleInDegrees-90);
    return {
        x: centerX + (radius * Math.cos(angleInRadians)),
        y: centerY + (radius * Math.sin(angleInRadians))
    };
}
export function cartesianToPolar(x, y){
	const distance = Math.sqrt(x*x + y*y);
	const radians = Math.atan2(y,x);
	return { 
		distance: distance, 
		radians: radians + (Math.PI/2)
	};
}


// - SVG -------------------------------------------
export function svgArc(x, y, radius, startAngle, endAngle){
    var start = polarToCartesian(x, y, radius, endAngle);
    var end = polarToCartesian(x, y, radius, startAngle);
    var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";
    var d = [
        "M", start.x, start.y, 
        "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
    ].join(" ");
    return d;       
}


// - DRAW -------------------------------------------
export function drawCurvePath(ctx, pts, _tension, _isClosed, _numOfSegments, _showPoints) {
    // -
    let tension         = _tension          || 0.5;
    let isClosed        = _isClosed         || false;
    let numOfSegments   = _numOfSegments    || 16;
    let showPoints      = _showPoints       || false;
    // -
    ctx.beginPath();
    // -
    drawLines(ctx, getCurvePoints(pts, tension, isClosed, numOfSegments));
    // -
    if (showPoints) {
        ctx.stroke();
        ctx.beginPath();
        // -
        for( let i=0; i<pts.length-1; i+=2 ){
            ctx.rect(pts[i]-2, pts[i+1]-2, 4, 4);
        }
    }
    // -
    ctx.fill();
}

export function getCurvePoints(pts, _tension, _isClosed, _numOfSegments) {
    // - Use input value if provided, or use a default value   
    let tension         = _tension          || 0.5;
    let isClosed        = _isClosed         || false;
    let numOfSegments   = _numOfSegments    || 16;

    let _pts = [], res = [],// clone array
        x, y,               // our x,y coords
        t1x, t2x, t1y, t2y, // tension vectors
        c1, c2, c3, c4,     // cardinal points
        st, t, i;           // steps based on num. of segments

    // clone array so we don't change the original
    _pts = pts.slice(0);

    // The algorithm require a previous and next point to the actual point array.
    // Check if we will draw closed or open curve.
    // If closed, copy end points to beginning and first points to end
    // If open, duplicate first points to befinning, end points to end
    if ( isClosed ) {
        _pts.unshift(pts[pts.length - 1]);
        _pts.unshift(pts[pts.length - 2]);
        _pts.unshift(pts[pts.length - 1]);
        _pts.unshift(pts[pts.length - 2]);
        _pts.push(pts[0]);
        _pts.push(pts[1]);
    
    }else{
        _pts.unshift(pts[1]);           //copy 1. point and insert at beginning
        _pts.unshift(pts[0]);
        _pts.push(pts[pts.length - 2]); //copy last point and append
        _pts.push(pts[pts.length - 1]);
    }

    // ok, lets start..

    // 1. loop goes through point array
    // 2. loop goes through each segment between the 2 pts + 1e point before and after
    for (i=2; i < (_pts.length - 4); i+=2) {
        for (t=0; t <= numOfSegments; t++) {

            // calc tension vectors
            t1x = (_pts[i+2] - _pts[i-2]) * tension;
            t2x = (_pts[i+4] - _pts[i]) * tension;

            t1y = (_pts[i+3] - _pts[i-1]) * tension;
            t2y = (_pts[i+5] - _pts[i+1]) * tension;

            // calc step
            st = t / numOfSegments;

            // calc cardinals
            c1 =   2 * Math.pow(st, 3)  - 3 * Math.pow(st, 2) + 1; 
            c2 = -(2 * Math.pow(st, 3)) + 3 * Math.pow(st, 2); 
            c3 =       Math.pow(st, 3)  - 2 * Math.pow(st, 2) + st; 
            c4 =       Math.pow(st, 3)  -     Math.pow(st, 2);

            // calc x and y cords with common control vectors
            x = c1 * _pts[i]    + c2 * _pts[i+2] + c3 * t1x + c4 * t2x;
            y = c1 * _pts[i+1]  + c2 * _pts[i+3] + c3 * t1y + c4 * t2y;

            //store points in array
            res.push(x);
            res.push(y);
        }
    }
    return res;
}

export function drawLines(ctx, pts) {
    ctx.moveTo(pts[0], pts[1]);
    // -
    for(let i=2; i<pts.length-1; i+=2){
        ctx.lineTo(pts[i], pts[i+1]);
    }
}


//- Functions
export function saveData2File(strData, filename) {
    var link = document.createElement('a');
    if (typeof link.download === 'string') {
        document.body.appendChild(link); //Firefox requires the link to be in the body
        link.download = filename;
        link.href = strData;
        link.click();
        document.body.removeChild(link); //remove the link when done

    } else {
        location.replace( strData );
    }
};

export function canvas2Png( _canvas, _name ){
	try {
		let imgData = _canvas.toDataURL( 'image/png' );
		saveData2File( imgData, `${_name}.png` );
	} catch (e) {
		console.log(e);
		return;
	}
};

export function obj2Json( _obj, _name ){
	const json = JSON.stringify( _obj );
    const file = new Blob([json], {type: 'application/json'});
    saveData2File( URL.createObjectURL(file), `${_name}.json` );
};

export function loadJsonFile( _func ){
	let input = document.createElement('input');
    input.type = 'file';
    input.onchange = e => { 
        // getting a hold of the file reference
        var file = e.target.files[0]; 

        // setting up the reader
        var reader = new FileReader();
        reader.readAsText(file,'UTF-8');

        // here we tell the reader what to do when it's done reading...
        reader.onload = (ev) => {
            _func( JSON.parse( ev.target.result ) );
        }
    }
    input.click();
};