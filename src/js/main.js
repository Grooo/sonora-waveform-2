import { drawCurvePath } from './functions'
import SimplexNoise from 'simplex-noise'
// - 
import { AudioContext } from 'standardized-audio-context'
// - 

let opts = {
	fps: 				60,
	iniTime: 			0,
	endTime: 			120,
	width: 				850,
	height: 			400,
	velUp: 				0.5,
	velDown: 			0.08,

	waves: {
		layers: 		2,
		points: 		32,
		sinusoidal: 	0.15913,
		sinusoidalPercent: 1,
		tension: 		0.5,
		incr: 			0.05,
		multiplier: 	0.7,
		color: 			'#ff002d',
		alpha: 			0.5,
		colorRGBA: 		'rgba(255,0,120, 0.5)',
		gradient: 		0.1,
		gradient2: 		1.0,
		color2: 		'#ff0f00',
		alpha2: 		0.3,
		colorRGBA2: 	'rgba(255,15,0, 0.25)',
		lineColor: 		'#fc0000',
		lineAlpha: 		1,
		lineColorRGBA: 	'rgba(252,0,0, 0)',
		lineWidth:    	0.5,
		blendMode: 		'screen'
	},

	simplex: {
		percent: 		0.3,
		vel:			-0.006,
		multiplier: 	0.07,
		interleaved: 	false
	},
	shadow: {
		blur: 			32.703,
		distance: 		-6.01,
		color: 			`#0a0000`,
		alpha: 			1,
		colorRGBA: 		'rgba(10,0,0, 1)'
	},

	blur: 				1.2,

	fade: 				0.2,

	volume: 			1
}


// - DOM
const waveform = document.querySelector('canvas#waveform');
const ctx = waveform.getContext('2d');
// -
const fileInput = document.querySelector('input#file')


// - Extra movement
const simplex = new SimplexNoise();

// -
let points = [], waves = [], 
	l = 0,
	percentFrequency = 0;
// -
let shapeIncr 	= 0,
	posX 		= 0,
	extraY  	= 0,
	incrPoint 	= 1;

// -
let win = { w:1024, h:1024 };
// -
function resizeCanvas(e){
	win.w = window.innerWidth;
	win.h = window.innerHeight;
	// -
	waveform.setAttribute('width', win.w);
	waveform.style.width = `${win.w}px`;
	// -
	waveform.setAttribute('height', opts.height);
	waveform.style.height = `${opts.height}px`;
	// -
	incrPoint = win.w / (opts.waves.points-1);

	// - Waves
	waves = [];
	// -
	for(let i=0; i<opts.waves.layers; i++){
		const totalPoints = (opts.waves.points+2)*2;
		points = new Array( totalPoints );
		points.fill(opts.height);
		// -
		points[ totalPoints-4] = opts.width;
		points[ totalPoints-3] = opts.height;
		points[ totalPoints-2] = 0;
		points[ totalPoints-1] = opts.height;
		// -
		waves.push({
			points: points,
			proxyPoints: [...points]
		});
	}
}
resizeCanvas();


// - Save file
function drawFrame( frequencyArray ) {
	ctx.save();
	ctx.clearRect(0,0, win.w, opts.height);

	// - Background gradient
	let gradient = null;
	// - Blur
	ctx.filter = `blur(${opts.blur}px)`;

	
	// - Waves
	waves.forEach( (wave, i) =>{
		ctx.globalCompositeOperation = opts.waves.blendMode;
		// -
		l = opts.waves.points * 2;
		percentFrequency = ( frequencyArray.length / 2 / (wave.points.length * waves.length) );
		
		// -
		let j 			= 0,
			maxY 		= 0,
			absolutePos = 0,
			velPos 		= 0,
			vel 		= 0;
			// -
		for( j=0; j<l; j+=2){
			// - x
			wave.points[j] = j*incrPoint*0.5;
			// - 
			absolutePos = (i*l)+j;

			// - Ajustamos con una sinusoidal para que los graves no destaquen tanto y se parezca mas al diseño
			shapeIncr = 1 - (Math.abs(Math.cos(j/(l*opts.waves.sinusoidal*2))) * opts.waves.sinusoidalPercent);

			// - Add extra movement
			posX += opts.simplex.vel;
			velPos = opts.simplex.interleaved && (i%2 === 0) ? absolutePos-posX : absolutePos+posX;
			extraY = 0.5 + (simplex.noise2D(velPos*opts.simplex.multiplier, 0) * opts.simplex.percent * 0.5);
			// - 
			/*
			wave.proxyPoints[j+1] = opts.height - (Math.random() * opts.height); 
			/*/
			wave.proxyPoints[j+1] = opts.height - (((frequencyArray[Math.floor(percentFrequency*absolutePos)]) * opts.height * (opts.waves.multiplier/opts.waves.layers))*shapeIncr*(1-(absolutePos*opts.waves.incr*-1))) * extraY * opts.volume;
			//*/
			// - if(j==0) console.log( wave.proxyPoints[j+1], wave.points[j+1]);
			vel = wave.proxyPoints[j+1] < wave.points[j+1] ? opts.velUp : opts.velDown;
			wave.points[j+1] += (wave.proxyPoints[j+1] - wave.points[j+1]) * vel; // - y
			// -
			// maxY = Math.max( maxY, wave.points[j+1]) || 0;
		}

		// - Draw
		gradient = ctx.createLinearGradient(0, 0, 0, opts.height );
		// Add three color stops
		gradient.addColorStop(0, opts.waves.colorRGBA );
		gradient.addColorStop(opts.waves.gradient, opts.waves.colorRGBA );
		gradient.addColorStop(opts.waves.gradient2, opts.waves.colorRGBA2 );
		gradient.addColorStop(1, opts.waves.colorRGBA2 );
		ctx.fillStyle = gradient;
		ctx.fill();		
		

		// - Stroke
		ctx.strokeStyle = opts.waves.lineColorRGBA;
		ctx.lineWidth = opts.waves.lineWidth;
		ctx.stroke();

		// - Drop Shadow
		ctx.shadowOffsetY = 0;
		ctx.shadowOffsetY = opts.shadow.distance * -1;
		ctx.shadowColor = opts.shadow.colorRGBA;
		ctx.shadowBlur = opts.shadow.blur;

		// -
		drawCurvePath(ctx, wave.points, opts.waves.tension, true, 8 );

		// -
		ctx.globalCompositeOperation = 'source-over';
		
	});
	
	// -
	ctx.restore();
}
	

// - Get audio
let audioCtx = null;


// - 
function drawWaveform(pcmdata, samplerate){
	let interval = 1000 / opts.fps;
	let step = Math.floor( samplerate / opts.fps );
	let index = opts.iniTime * samplerate;
	// -
	console.log( samplerate, index, opts.endTime*samplerate, pcmdata.length, step );
	// -
	dataIni = new Date();
	//loop through song in time with sample rate
	let samplesound = setInterval( ()=> {
		// -  console.log( index );
		if (index >= Math.min(pcmdata.length, opts.endTime*samplerate) ) {
			clearInterval(samplesound);
			console.log("End audio");
			// - Convert video
			// convertVideo();
			return;
		}
		// - 
		drawFrame( pcmdata.slice(index, index+step) );
		/*/ - En back sumamos el step
		index += step;
		/*/ // - En Front miramos exactamente donde se encuentra el audio (y ajustamos)
		index = Math.max(0, (samplerate * audioCtx.currentTime) - (samplerate*0.5) );
		//*/
	}, interval, pcmdata );
}
// - 

// - Filereader
function handleFileSelect(eHandler) {
	let files = eHandler.target.files;
	let freader = new FileReader();
	// - 
    freader.onload = function(e) {
		// - console.log( e );
		audioCtx = new AudioContext();
		audioCtx.decodeAudioData(e.target.result, function(audioBuffer) {
			opts.endTime = audioBuffer.duration;
			// - 
			drawWaveform( audioBuffer.getChannelData(0), audioBuffer.sampleRate );

			// - Play sound
			let source = audioCtx.createBufferSource();
			source.connect(audioCtx.destination);
			source.buffer = audioBuffer;
			source.start(opts.iniTime);
			//*/

			// - desactivamos el input
			fileInput.remove();

		}, function(err) { throw err })
    };
	// -
    freader.readAsArrayBuffer(files[0]);
}

fileInput.addEventListener('change', handleFileSelect, false);

// - Init
window.addEventListener('resize', resizeCanvas);